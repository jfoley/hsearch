package edu.umass.cs.jfoley.hsearch;

import java.util.List;

/**
 * @author jfoley
 */
public interface QuestionAsQuery {
  /** returns a text representation used for matching to categories. */
  public String getTextRepresentation();

  /** returns true if it should be used for training */
  public boolean isTrainingPoint();
  /** returns a list of relevant node ids from the hierarchy, only used if it's training. */
  public List<String> getRelevantHierarchyNodes();
  /** returns a unique identifier, only used if it's training. */
  public String getId();
}
