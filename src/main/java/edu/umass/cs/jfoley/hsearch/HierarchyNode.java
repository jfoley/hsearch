package edu.umass.cs.jfoley.hsearch;

import java.util.*;

/**
 * @author jfoley
 */
public class HierarchyNode {
  public final String id;
  public String description;

  public HierarchyNode root;
  public HierarchyNode parent;
  public Set<HierarchyNode> children;
  boolean setup;

  public HierarchyNode(String id, String description) {
    this.id = id;
    this.description = description;

    // filled in before initialize();
    setup = false;
    this.root = null;
    this.parent = null;
    this.children = new HashSet<>();
  }

  /** set a reference to the parent node */
  public void setParent(HierarchyNode parent) {
    assert(!setup);
    this.parent = parent;
  }
  public void addChild(HierarchyNode hn) {
    assert(!setup);
    this.children.add(hn);
  }

  /** implemented so that we can participate in a set by id. */
  @Override
  public int hashCode() {
    return this.id.hashCode();
  }

  /** implemented so that we can participate in a set by id. */
  @Override
  public boolean equals(Object other) {
    if(other instanceof HierarchyNode) {
      return this.id.equals(((HierarchyNode) other).id);
    }
    return false;
  }

  /** true if this doesn't have children. */
  public boolean isLeaf() {
    assert(setup);
    return children.isEmpty();
  }

  /** Call this after setting the parent on all nodes. */
  public void initialize() {
    assert(!setup);
    assert(parent != null);
    assert(children != null);

    // Find root:
    for(HierarchyNode ptr = this; ptr != null; ptr = ptr.parent) {
      root = ptr;
    }
    setup = true;
  }

  /** Return a list of descendants, including this node */
  public List<HierarchyNode> getDescendants() {
    assert(setup);
    List<HierarchyNode> output = new ArrayList<>();
    LinkedList<HierarchyNode> frontier = new LinkedList<>();
    frontier.add(this);
    while(true) {
      if(frontier.isEmpty()) break;

      HierarchyNode ptr = frontier.pop();
      output.add(ptr);
      for (HierarchyNode child : ptr.children) {
        frontier.add(child);
      }
    }

    return output;
  }

  /** Return a list of ancestors, including this node */
  public List<HierarchyNode> getAncestors() {
    assert(setup);
    List<HierarchyNode> ancestors = new ArrayList<>();
    for(HierarchyNode ptr = this; ptr != null; ptr = ptr.parent) {
      ancestors.add(ptr);
    }
    return ancestors;
  }


}
