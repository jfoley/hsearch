package edu.umass.cs.jfoley.hsearch;

import java.util.List;

/**
 * @author jfoley
 */
public class SimpleQuestion implements QuestionAsQuery {
  private final String text;

  public SimpleQuestion(String textOrHTML) {
    assert(textOrHTML != null);
    this.text = textOrHTML;
  }
  @Override
  public String getTextRepresentation() {
    return text;
  }

  @Override
  public String toString() {
    return "SimpleQuestion("+text+")";
  }

  @Override
  public boolean isTrainingPoint() {
    return false;
  }

  @Override
  public List<String> getRelevantHierarchyNodes() {
    throw new UnsupportedOperationException();
  }

  @Override
  public String getId() {
    throw new UnsupportedOperationException();
  }
}
