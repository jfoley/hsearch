package edu.umass.cs.jfoley.hsearch;

import org.lemurproject.galago.utility.Parameters;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Example {

  public static Map<String, HierarchyNode> createHierarchyFromJSON(List<Parameters> hjson) {
    Map<String, HierarchyNode> nodes = new HashMap<>();

    /** create a node with each description and id */
    for (Parameters h : hjson) {
      HierarchyNode hn = new HierarchyNode(h.getString("id"), h.getString("description"));
      nodes.put(hn.id, hn);
    }

    /** set up parent pointers and children sets */
    for (Parameters h : hjson) {
      HierarchyNode hn = nodes.get(h.getString("id"));
      if(h.containsKey("parent")) {
        HierarchyNode pn = nodes.get(h.getString("parent"));
        hn.setParent(pn);
        pn.addChild(hn);
      }
    }

    /**
     * Call initialize so that the nodes can find their roots;
     * This needs to happen after all parent links are setup.
     */
    for (HierarchyNode node : nodes.values()) {
      node.initialize();
    }

    return nodes;
  }

  public static List<QuestionAsQuery> createQueriesFromJSON(List<Parameters> qjson) {
    List<QuestionAsQuery> output = new ArrayList<>();
    for (Parameters qj : qjson) {
      if(qj.isString("qid") && qj.isList("judgments")) {
        String id = qj.getString("qid");
        String text = qj.getString("text");
        List<String> judgments = qj.getAsList("judgments", String.class);
        TrainingQuestion q = new TrainingQuestion(id, text, judgments);
        output.add(q);
      } else {
        String text = qj.getString("text");
        SimpleQuestion q = new SimpleQuestion(text);
        output.add(q);
      }
    }
    return output;
  }

  public static void main(String[] args) throws IOException {
    Parameters argp = Parameters.parseArgs(args);

    List<Parameters> qjson = Parameters.parseFile(argp.get("questions-json", "example_data/gov-questions.json")).getAsList("data", Parameters.class);
    List<Parameters> hjson = Parameters.parseFile(argp.get("hierarchy-json", "example_data/gov-hierarchy.json")).getAsList("data", Parameters.class);

    Map<String, HierarchyNode> hierarchy = createHierarchyFromJSON(hjson);
    List<QuestionAsQuery> questions = createQueriesFromJSON(qjson);

    System.out.println(hierarchy);
    System.out.println(questions);

    assert(!hierarchy.isEmpty());
    assert(!questions.isEmpty());

  }
}
