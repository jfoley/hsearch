package edu.umass.cs.jfoley.hsearch;

import java.util.Collections;
import java.util.List;

/**
 * Object to be categorized into the hierarchy.
 * @author jfoley
 */
public class TrainingQuestion implements QuestionAsQuery {
  public final String id;
  public final String text;
  public final List<String> relevantHierarchyNodes;

  public TrainingQuestion(String id, String textOrHTML, List<String> relevantHierarchyNodes) {
    assert(!relevantHierarchyNodes.isEmpty());
    assert(id != null);
    assert(textOrHTML != null);

    this.id = id;
    this.text = textOrHTML;
    this.relevantHierarchyNodes = relevantHierarchyNodes;
  }

  @Override
  public boolean isTrainingPoint() {
    return true;
  }

  /** This returns the list of relevant hierarchy nodes. */
  @Override
  public List<String> getRelevantHierarchyNodes() {
    return relevantHierarchyNodes;
  }

  /** This returns the text or html representation of a question. */
  @Override
  public String getTextRepresentation() {
    return text;
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public String toString() {
    return "TrainingQuestion("+id+", "+text+", "+relevantHierarchyNodes+")";
  }
}
